@extends('layout.main')

@section('meta')
    <title>Ecom San Luis | Invitados</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="Conoce a grandes artistas de doblaje, cosplayers e ilustradores." name="description"/>
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div id="invitados">
        <section class="container">

            <section class="container-fluid features">
                <h1>Invitados ECOM 2016</h1>
                <div class="">
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-01.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Daniss Doll</h1>
                        <h2>Cosplayer</h2>
                        <p><span>Diciembre 16, 17 y 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-02.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Lorraine Cosplay</h1>
                        <h2>Cosplayer</h2>
                        <p><span>Diciembre 16, 17 y 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-03.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Nintendo Girl Paprika</h1>
                        <h2>Vlogger/Cosplayer</h2>
                        <p><span>Diciembre 16, 17 y 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-04.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Mariam</h1>
                        <h2>Conductora Invitada</h2>
                        <p><span>Diciembre 16, 17 y 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-05.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>AGARWEN</h1>
                        <h2>Ilustrador</h2>
                        <p><span>Diciembre 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-06.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Diana Pérez</h1>
                        <h2>Doblaje</h2>
                        <p><span>Diciembre 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-07.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Analiz Sánchez</h1>
                        <h2>Doblaje</h2>
                        <p><span>Diciembre 17</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-08.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>Carla Castañeda</h1>
                        <h2>Doblaje</h2>
                        <p><span>Diciembre 17</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/invitados-09.png')}}" alt="Invitados ECOM 2016" class="img-responsive">
                        <h1>I LOVE COSPLAY</h1>
                        <h2>Medios</h2>
                        <p><span>Diciembre 16, 17 y 18</span></p>
                    </div>
                </div>
            </section>
            <section class="container-fluid promo">
                <div class="">
                    <div class="col-sm-3">
                        <h2>Edición limitada</h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Entrada los 3 días</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Print edición especial</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Autógrafos asegurados</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Asientos preferenciales</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> ¡Sin Filas!</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 precio">
                        <h1>V.I.F. Pass</h1>
                        <p><span>$300</span></p>
                        <p><small>Oishii Maid Café | Iturbide 915</small><br><small>La Mole Comics | Nicolás Zapata 369</small></p>
                    </div>
                    <div class="col-sm-3">
                        <img src="{{asset('images/flaquita-sexy.png')}}" alt="playera edición especial" class="img-responsive center-block">
                    </div>
                </div>
            </section>
            <section class="container-fluid patroncitos">
                <div class="">
                    <div class="">
                        <h2>Patrocinadores</h2>
                        <ul>
                            <li><img src="{{asset('images/logo-minichat.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-dbak.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-plaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-frikiplaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-lovecosplay.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-tacho.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop