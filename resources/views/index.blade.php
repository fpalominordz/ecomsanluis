@extends('layout.main')

@section('meta')
    <title>Ecom San Luis | Inicio</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="Expo Comics San Luis, atrévete a vivirla" name="description"/>
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div id="index">
        <section class="container">
            <div id="myCarousel" class="carousel slide " data-ride="carousel">
                <!-- Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="{{asset('images/ecom-prime.jpg')}}" alt="ECOM Diciembre 2016" class="img-responsive">
                        <!--<div class="container overlayTxt">
                            <div class="hidden-xs col-sm-8">
                                <h1>Atrévete a vivirla</h1>
                                <h2>Comics, Anime, Películas, videojuegos y más</h2>
                            </div>
                        </div>-->
                    </div>
                    <div class="item">
                        <img src="{{asset('images/ecom-invitados.jpg')}}" alt="ECOM Diciembre 2016 invitados" class="img-responsive">
                        <!--<div class="container overlayTxt">
                            <div class="hidden-xs col-sm-8">
                                <h1>Atrévete a vivirla</h1>
                                <h2>Comics, Anime, Películas, videojuegos y más</h2>
                            </div>
                        </div>-->
                    </div>
                    <div class="item">
                        <img src="{{asset('images/ecom-VIF.jpg')}}" alt="ECOM Diciembre 2016 V.I.F. Pass" class="img-responsive">
                       <!-- <div class="container overlayTxt">
                            <div class="hidden-xs col-sm-8">
                                <h1>Atrévete a vivirla</h1>
                                <h2>Comics, Anime, Películas, videojuegos y más</h2>
                            </div>
                        </div>-->
                    </div>
                </div>

                <!-- Left and right controls-->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <section class="container-fluid promo">
                <div class="">
                    <div class="col-sm-4">
                        <h2>Te esperamos</h2>
                        <ul>
                            <li> Diciembre</li>
                            <li> 16, 17 y 18</li>
                            <li> de 11am a 8pm</li>
                        </ul>
                    </div>
                    <div class="col-sm-4 precio">
                        <h2>Entrada General</h2>
                        <p><span>$65</span></p>
                        <p><small>Con Cosplay: $45</small></p>
                    </div>
                    <div class="col-sm-4">
                        <h2>Edificio de la CTM</h2>
                        <ul>
                            <li> Constitución 1035</li>
                            <li> Barrio de San Sebastián</li>
                            <li> Zona Centro, San Luis Potosí</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="container-fluid features">
                <div class="">
                    <div class="col-sm-4">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <h2>Invitados especiales</h2>
                        <p>Conoce a grandes artistas de doblaje, cosplayers e ilustradores.</p>
                    </div>
                    <div class="col-sm-4">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                        <h2>Concursos</h2>
                        <p>Participa solo o con tus amigos en los concursos que tenemos para ti.</p>
                    </div>
                    <div class="col-sm-4">
                        <i class="fa fa-bolt" aria-hidden="true"></i>
                        <h2>Talleres</h2>
                        <p>Interesantes pláticas y talleres de nuestros invitados y colectivos.</p>
                    </div>
                    <div class="col-sm-4">
                        <i class="fa fa-film" aria-hidden="true"></i>
                        <h2>Proyecciones</h2>
                        <p>Funciones especiales con solo las mejores películas del género.</p>
                    </div>
                    <div class="col-sm-4">
                        <i class="fa fa-cutlery" aria-hidden="true"></i>
                        <h2>Área de comida</h2>
                        <p>Atáscate como Gokú en la sección de alimentos, no olvides pasar por Oishii :3</p>
                    </div>
                    <div class="col-sm-4">
                        <i class="fa fa-gift" aria-hidden="true"></i>
                        <h2>Comercio</h2>
                        <p>Cómprale cosas a la waifu :D... o invierte en ti, anda, lo vales.</p>
                    </div>
                </div>
            </section>
            <section class="container-fluid promo">
                <div class="">
                    <div class="col-sm-3">
                        <h2>Edición limitada</h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Entrada los 3 días</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Print edición especial</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Autógrafos asegurados</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Asientos preferenciales</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> ¡Sin Filas!</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 precio">
                        <h1>V.I.F. Pass</h1>
                        <p><span>$300</span></p>
                        <p><small>Oishii Maid Café | Iturbide 915</small><br><small>La Mole Comics | Nicolás Zapata 369</small></p>
                    </div>
                    <div class="col-sm-3">
                        <img src="{{asset('images/flaquita-sexy.png')}}" alt="playera edición especial" class="img-responsive center-block">
                    </div>
                </div>
            </section>
            <section class="container-fluid patroncitos">
                <div class="">
                    <div class="">
                        <iframe src="http://www.youtube.com/embed/?listType=user_uploads&list=ECOMSLP" width="480" height="400"></iframe>
                    </div>
                </div>
            </section>
            <section class="container-fluid patroncitos">
                <div class="">
                    <div class="">
                        <h2>Patrocinadores</h2>
                        <ul>
                            <li><img src="{{asset('images/logo-minichat.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-dbak.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-plaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-frikiplaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-lovecosplay.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-tacho.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop