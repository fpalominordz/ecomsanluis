<div class="container-fluid logBar">
    <div class="container">
        <h2>anime, comics, concursos, cosplay, doblaje, moda, Kpop, +</h2>
    </div>
</div>
<header class="container-fluid">
    <div class="container menu">
        <div id="main-menu" class="navbar navbar-custom" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="logo" href="{{route('inicio')}}">
                        <img src="{{asset('images/ecom-logo.png')}}" alt="Contapp logo" class="img-responsive">
                    </a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="collapse navbar-collapse navbar-menubuilder">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="{{route('inicio')}}">ECOM 2016</a></li>
                        <li><a href="{{route('invitados')}}">Invitados</a></li>
                        <li><a href="{{route('eventos')}}">Eventos</a></li>
                        <li><a href="{{route('programa')}}">Programa</a></li>
                        <li><a href="{{route('contacto')}}">Contacto</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="https://www.facebook.com/ECOMSANLUIS/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/Ecomsanluis"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.youtube.com/user/ECOMSLP"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>