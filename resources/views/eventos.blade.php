@extends('layout.main')

@section('meta')
    <title>Ecom San Luis | Eventos</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="Participa solo o con tus amigos en los concursos que tenemos para ti." name="description"/>
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div id="eventos">
        <section class="container">

            <section class="container-fluid features">
                <h1>Eventos ECOM 2016</h1>
                <div class="">
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-01.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Le Petit Cosplay</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-02.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Youngster Cosplay</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 17</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-03.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Pasarela Harayuku</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 16</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-04.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Cosplay Grupal</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 16</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-05.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Coreografía</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-06.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Winter Cosplay</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 18</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-07.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Karaoke</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 16</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-08.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Lipsynk Battle</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 16</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-09.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Rumbo a IMAGINA</h1>
                        <h2>Concurso nacional</h2>
                        <p><span>Diciembre 17</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-10.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Taller de Tintas</h1>
                        <h2>Curso por AGARWEN</h2>
                        <p><span>Diciembre 17</span></p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('images/eventos-11.png')}}" alt="Eventos ECOM 2016" class="img-responsive">
                        <h1>Torneo Pokémon</h1>
                        <h2>Concurso</h2>
                        <p><span>Diciembre 16, 17 y 18</span></p>
                    </div>
                </div>
            </section>
            <section class="container-fluid promo">
                <div class="">
                    <div class="col-sm-3">
                        <h2>Edición limitada</h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Entrada los 3 días</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Print edición especial</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Autógrafos asegurados</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Asientos preferenciales</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> ¡Sin Filas!</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 precio">
                        <h1>V.I.F. Pass</h1>
                        <p><span>$300</span></p>
                        <p><small>Oishii Maid Café | Iturbide 915</small><br><small>La Mole Comics | Nicolás Zapata 369</small></p>
                    </div>
                    <div class="col-sm-3">
                        <img src="{{asset('images/flaquita-sexy.png')}}" alt="playera edición especial" class="img-responsive center-block">
                    </div>
                </div>
            </section>
            <section class="container-fluid patroncitos">
                <div class="">
                    <div class="">
                        <h2>Patrocinadores</h2>
                        <ul>
                            <li><img src="{{asset('images/logo-minichat.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-dbak.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-plaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-frikiplaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-lovecosplay.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-tacho.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop