@extends('layout.main')

@section('meta')
    <title>Ecom San Luis | Contacto</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="Conoce a grandes artistas de doblaje, cosplayers e ilustradores." name="description"/>
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div id="programa">
        <section class="container">

            <section class="container-fluid programaTable">
                <h1>Contacto</h1>
                <div class="">
                    <div class="col-sm-4">
                        <h3>Edificio de la CTM</h3>
                        <p>Av. Constitución 1035, Barrio de San Sebastian, 78349 San Luis, S.L.P.</p>
                        <a href="mailto:contacto@ecomsanluis.com.mx"><i class="fa fa-envelope"></i>Contacto ECOM</a>
                    </div>

                    <div class="col-sm-8">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3695.459831265627!2d-100.97366648504807!3d22.146561885403646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842aa2049b3041f3%3A0x4dc43c90da9a8cd0!2sAv.+Constituci%C3%B3n+1035%2C+Barrio+de+San+Sebastian%2C+78349+San+Luis%2C+S.L.P.!5e0!3m2!1ses!2smx!4v1481571727174" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </section>
            <section class="container-fluid promo">
                <div class="">
                    <div class="col-sm-3">
                        <h2>Edición limitada</h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Entrada los 3 días</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Print edición especial</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Autógrafos asegurados</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Asientos preferenciales</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> ¡Sin Filas!</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 precio">
                        <h1>V.I.F. Pass</h1>
                        <p><span>$300</span></p>
                        <p><small>Oishii Maid Café | Iturbide 915</small><br><small>La Mole Comics | Nicolás Zapata 369</small></p>
                    </div>
                    <div class="col-sm-3">
                        <img src="{{asset('images/flaquita-sexy.png')}}" alt="playera edición especial" class="img-responsive center-block">
                    </div>
                </div>
            </section>
            <section class="container-fluid patroncitos">
                <div class="">
                    <div class="">
                        <h2>Patrocinadores</h2>
                        <ul>
                            <li><img src="{{asset('images/logo-minichat.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-dbak.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-plaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-frikiplaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-lovecosplay.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-tacho.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop