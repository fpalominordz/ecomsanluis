<!DOCTYPE html>
<!-- start: HEAD -->
<head>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{asset('android-chrome-192x192.png')}}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{asset('favicon-16x16.png')}}" sizes="16x16">
    <link rel="manifest" href="{{asset('manifest.json')}}">
    <link rel="mask-icon" href="{{asset('safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
@yield('meta')

<!-- start: MAIN CSS-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,400&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- end: MAIN CSS -->
    <!-- google console verification-->

    <!-- font awesome -->
    <script src="https://use.fontawesome.com/f2ed23dd41.js"></script>
    @yield('styles')
</head>
<!-- end: HEAD -->
<body>

@yield('header')
@yield('content')
@yield('footer')


<!-- start: MAIN JAVASCRIPTS -->
<!-- google jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- bootstrap js -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- sweetalert js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<!-- google analytics -->
<!-- end: MAIN JAVASCRIPTS -->
@yield('scripts')

</body>