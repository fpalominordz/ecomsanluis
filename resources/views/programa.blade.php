@extends('layout.main')

@section('meta')
    <title>Ecom San Luis | Programa</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="Conoce a grandes artistas de doblaje, cosplayers e ilustradores." name="description"/>
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet -->
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('header')
    @include('partials.header')
@stop
@section('content')
    <div id="programa">
        <section class="container">

            <section class="container-fluid programaTable">
                <h1>Programa ECOM 2016</h1>
                <div class="">
                    <div class="col-sm-4">
                        <table style="width:100%">
                            <tr>
                                <th colspan="2"><h3>Viernes 16</h3></th>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Escenario Chico</strong></td>
                            </tr>
                            <tr>
                                <td>12:00</td><td>Inauguración</td>
                            </tr>
                            <tr>
                                <td>13:00</td><td>Concursos Varios</td>
                            </tr>
                            <tr>
                                <td>16:00</td><td>Confirmaciones Karaoke</td>
                            </tr>
                            <tr>
                                <td>16:00 </td><td>Conferencia con  Lorraine Cosplay: Tecnologías aplicadas al Cosplay</td>
                            </tr>
                            <tr>
                                <td>16:40</td><td>Concurso Karaoke</td>
                            </tr>
                            <tr>
                                <td colspan="2">Intermedio Musical</td>
                            </tr>
                            <tr>
                                <td>18:00</td><td>Conferencia con AGARWEN</td>
                            </tr>
                            <tr>
                                <td>16:40</td><td>Concurso Karaoke</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Auditorio</strong></td>
                            </tr>
                            <tr>
                                <td>15:00</td><td>Proyección</td>
                            </tr>
                            <tr>
                                <td>17:30</td><td>Cosplay Grupal</td>
                            </tr>
                            <tr>
                                <td>18:00</td><td>Concurso Pasarela Harajuku</td>
                            </tr>
                            <tr>
                                <td>18:30</td><td>Concurso Lipsync Battle</td>
                            </tr>
                            <tr>
                                <td>19:00</td><td>Sotako Anime Rock Band</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Explanada</strong></td>
                            </tr>
                            <tr>
                                <td>17:00 - 20:00</td><td>Torneo Pokémon</td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-sm-4">
                        <table style="width:100%">
                            <tr>
                                <th colspan="2"><h3>Sábado 17</h3></th>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Escenario Chico</strong></td>
                            </tr>
                            <tr>
                                <td>12:00</td><td>Concursos Trivias</td>
                            </tr>
                            <tr>
                                <td>13:30</td><td>FRIKPARDY</td>
                            </tr>
                            <tr>
                                <td>15:00</td><td>Taller Tintas por AGARWEN (segundo piso)</td>
                            </tr>
                            <tr>
                                <td>15:00</td><td>Conferencia con PAPRIKA</td>
                            </tr>
                            <tr>
                                <td>16:00</td><td>Jenga!</td>
                            </tr>
                            <tr>
                                <td colspan="2">Intermedio Musical</td>
                            </tr>
                            <tr>
                                <td>17:00</td><td>Conferencia con  Daniss Doll </td>
                            </tr>
                            <tr>
                                <td>18:00</td><td>Performance Steampunk</td>
                            </tr>
                            <tr>
                                <td>18:30</td><td>Subastas</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Auditorio</strong></td>
                            </tr>
                            <tr>
                                <td>12:00</td><td>Proyección</td>
                            </tr>
                            <tr>
                                <td>14:00</td><td>Confirmaciones IMAGINA</td>
                            </tr>
                            <tr>
                                <td>14:40</td><td>Concurso Youngster Cosplay</td>
                            </tr>
                            <tr>
                                <td>15:00</td><td>Concurso IMAGINA</td>
                            </tr>
                            <tr>
                                <td colspan="2">Intermedio Musical</td>
                            </tr>
                            <tr>
                                <td>16:30</td><td>Conferencia Analis y Carla Castañeda </td>
                            </tr>
                            <tr>
                                <td>17:30 </td><td>Sesión de Autógrafos</td>
                            </tr>
                            <tr>
                                <td>19:00</td><td>Presentación</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Explanada</strong></td>
                            </tr>
                            <tr>
                                <td>11:00 - 20:00</td><td>Torneo Pokémon</td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-sm-4">
                        <table style="width:100%">
                            <tr>
                                <th colspan="2"><h3>Domingo 18</h3></th>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Escenario Chico</strong></td>
                            </tr>
                            <tr>
                                <td>12:00</td><td>Concurso Trivias </td>
                            </tr>
                            <tr>
                                <td>13:00</td><td>Basta Friki </td>
                            </tr>
                            <tr>
                                <td>14:00</td><td>Concurso “Abre tu Frikiregalo”</td>
                            </tr>
                            <tr>
                                <td>15:00</td><td>Conferencia con Chepe Rios </td>
                            </tr>
                            <tr>
                                <td>16:00</td><td>Paper costume:  “Arma tu duende”</td>
                            </tr>
                            <tr>
                                <td>17:00</td><td>Conferencia “La magia del cómic digital“ por GYRHS Clase muestra de cómo crear una página original usando Manga Studio</td>
                            </tr>
                            <tr>
                                <td>19:00</td><td>Subastas</td>
                            </tr>
                            <tr>
                                <td>20:00</td><td>Clausura</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Auditorio</strong></td>
                            </tr>
                            <tr>
                                <td>13:30</td><td>Confirmaciones “Winter cosplay”</td>
                            </tr>
                            <tr>
                                <td>14:15</td><td>Concurso Le Petite Cosplay</td>
                            </tr>
                            <tr>
                                <td>14:40</td><td>Concurso “Winter Cosplay”</td>
                            </tr>
                            <tr>
                                <td>16:30</td><td>Conferencia Diana Pérez</td>
                            </tr>
                            <tr>
                                <td>17:30</td><td>Sesión de Autógrafos</td>
                            </tr>
                            <tr>
                                <td>17:30</td><td>Confirmaciones coreografías</td>
                            </tr>
                            <tr>
                                <td>18:00</td><td>Concurso Coreografías</td>
                            </tr>
                            <tr>
                                <td>20:00</td><td>Clausura</td>
                            </tr>
                            <tr>
                                <td colspan="2"><strong>Explanada</strong></td>
                            </tr>
                            <tr>
                                <td>11:00 - 20:00</td><td>Torneo Pokémon</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </section>
            <section class="container-fluid promo">
                <div class="">
                    <div class="col-sm-3">
                        <h2>Edición limitada</h2>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Entrada los 3 días</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Print edición especial</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Autógrafos asegurados</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Asientos preferenciales</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> ¡Sin Filas!</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 precio">
                        <h1>V.I.F. Pass</h1>
                        <p><span>$300</span></p>
                        <p><small>Oishii Maid Café | Iturbide 915</small><br><small>La Mole Comics | Nicolás Zapata 369</small></p>
                    </div>
                    <div class="col-sm-3">
                        <img src="{{asset('images/flaquita-sexy.png')}}" alt="playera edición especial" class="img-responsive center-block">
                    </div>
                </div>
            </section>
            <section class="container-fluid patroncitos">
                <div class="">
                    <div class="">
                        <h2>Patrocinadores</h2>
                        <ul>
                            <li><img src="{{asset('images/logo-minichat.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-dbak.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-plaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-frikiplaza.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-lovecosplay.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                            <li><img src="{{asset('images/logo-tacho.png')}}" alt="playera edición especial" class="img-responsive center-block"></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    </div>
@stop

@section('footer')
    @include('partials.footer')
@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop