<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('inicio');
Route::get('inicio', function () {
    return view('index');
})->name('inicio');
Route::get('nosotros', function () {
    return view('nosotros');
})->name('nosotros');
Route::get('invitados', function () {
    return view('invitados');
})->name('invitados');
Route::get('eventos', function () {
    return view('eventos');
})->name('eventos');
Route::get('programa', function () {
    return view('programa');
})->name('programa');
Route::get('contacto', function () {
    return view('contacto');
})->name('contacto');